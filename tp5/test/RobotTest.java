import org.junit.*;
import static org.junit.Assert.*;

import factory.Robot;   
import factory.util.*;

import jdk.jfr.Timestamp;
//import sun.jvm.hotspot.utilities.AssertionFailure;

public class RobotTest {	

    @Test
    public void robotCarryNoBoxWhenCreated() {
	Robot robbie = new Robot();
	assertFalse(robbie.carryingBox());
    }

    @Test
    public void robotCanTakeBoxIfNotCarrying(){
    Robot robbie = new Robot();
    Box boite = new Box(20);
    assertFalse(robbie.carryingBox());
    robbie.take(boite);
    assertTrue(robbie.carryingBox());
    }

    @Test 
    public void robotCannotTakeBoxIfAlreadyCarrying(){
    Robot robbie = new Robot();
    Box boite = new Box(20);
    robbie.take(boite);
    assertTrue(robbie.carryingBox());
    Box boite2 = new Box(30);
    robbie.take(boite2);
    assertNotSame(boite2, robbie.getCarriedBox());
    }
    
    @Test
    public void robotPutOnTestTrue(){
    Robot robbie = new Robot();
    Box boite = new Box(20);
    ConveyerBelt tapis = new ConveyerBelt(100);
    robbie.take(boite);
    assertTrue(robbie.carryingBox());
    assertTrue(robbie.putOn(tapis));
    assertFalse(robbie.carryingBox());
    }

    @Test
    public void robotPutOnTestNotCarryingBox(){
    Robot robbie = new Robot();
    Box boite = new Box(20);
    ConveyerBelt tapis = new ConveyerBelt(100);
    assertFalse(robbie.carryingBox());
    assertFalse(robbie.putOn(tapis));
    }

    @Test
    public void robotPutOnTestBoxTooHeavy(){
    Robot robbie = new Robot();
    Box boite = new Box(20);
    ConveyerBelt tapis = new ConveyerBelt(10);
    robbie.take(boite);
    assertFalse(robbie.putOn(tapis));
    assertTrue(robbie.carryingBox());
    assertSame(20, boite.getWeight());
    }

    @Test
    public void robotPutOnTestConveyerBeltFull(){
    Robot robbie = new Robot();
    Box boite = new Box(20);
    Box boite2 = new Box(20);
    Box boite3 = new Box(20);
    ConveyerBelt tapis = new ConveyerBelt(100);

    robbie.take(boite);
    robbie.putOn(tapis);
    robbie.take(boite2);
    robbie.putOn(tapis);

    robbie.take(boite3);
    assertFalse(robbie.putOn(tapis));
    assertTrue(robbie.carryingBox());
    assertSame(20, boite3.getWeight());
    }


    // ---Pour permettre l'exécution des test----------------------
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(RobotTest.class);
    }

}
